# gulp-data-json

![](https://img.shields.io/npm/v/@loicpetitdev/gulp-data-json.svg?label=gulp-data-json&style=popout)
![](https://img.shields.io/npm/l/@loicpetitdev/gulp-data-json.svg?style=popout)
![](https://img.shields.io/bundlephobia/min/@loicpetitdev/gulp-data-json.svg?style=popout)

[NPM package](https://www.npmjs.com/package/@loicpetitdev/gulp-data-json) to provide data to gulp-data from json files.

## Install

Install the npm package :

```
npm install --save-dev @loicpetitdev/gulp-data-json
```

## Usage

The package must be used with **gulp** and **gulp-data** packages.

### Example

**gulp-compile-handlebars** package supports **gulp-data** plugin to inject data in the handlebars templates. Get HTML files from *src/pages*, then used **gulp-data** to loop through each file. Use the gulp-data-json function to get the JSON associated file of each HTML file, then **gulp-data** module will give these data to the next plugin : **gulp-compile-handlebars**.

By default, each JSON file must be at the same location with the same name of the associated HTML file. The JSON file is optionnal. Exemple :
* src
  * pages
    * index.html
    * index.json
    * about.html

Only *index.html* will get data.

The *globalData* object is for the **gulp-compile-handlebars** module which enable to inject in every page global data, merged with data from the JSON if existing.

```
// in gulpfile.js
const { src, dest, series, parallel, watch } = require('gulp')
const data = require('gulp-data')
const jsondata = require('./scripts/gulp-data-json')
const handlebars = require('gulp-compile-handlebars')

function compile(){
    // default json data options
    const jsonDataOptions = {
        debug: false,
        src: null,
        relativeFrom: null
    }
    // handlebars global data
    const globalData = {
        debug: false,
        title: "Handlebars"
    }
    // handlebars options
    const handlebarsOptions = {
        batch: ['src/partials']
    }
    // task
    return src('src/pages/*.html')
            .pipe(data(jsondata(jsonDataOptions)))
            .pipe(handlebars(globalData, handlebarsOptions))
            .pipe(dest('dist'))
}

```

### Debug option

*{debug: true}* is used to display log in order to see what happened in the module.

### Src option

*{src: '/my/path'}* is used to search JSON files in the folder */my/path* instead of next the file.

Example :

* project
  * templates
    * file1.html
    * sub
      * file2.html
  * json
    * file1.json
    * file2.json

*Files with same name will use the same json file !*

### RelativeFrom option

**required src option**

To avoid having several files with the same to use the same json file in the src directory, use relativeFrom option.

In the folloing example, *src* is json and *relativeFrom* is templates :

* project
  * templates
    * file1.html
    * sub
      * file2.html
  * json
    * file1.json
    * sub
      * file2.json

## Repository

**src** contains the module code. Unit tests are spec files.

**test** contains integration tests.

**.vscode** contains the *Visual Studio Code* configuration to debug tests.

## NPM package

There are only development dependencies to test the module.

Scripts

* **test:unit** runs the unit tests
* **test:int** runs the integration tests
* **test** runs all tests
* **watch:unit** watch adn runs the unit tests
* **watch:int** watch and runs the integration tests
* **watch** watch and runs all tests
* **debug** is used to debug the tests, used in the Visual Studio Code configuration
