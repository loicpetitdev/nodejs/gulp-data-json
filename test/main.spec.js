const expect = require('chai').expect
const assert = require('stream-assert')
const path = require('path')
const gulp = require('gulp')
const data = require('gulp-data')
const debug = require('gulp-debug')
const json = require('..')

function getExpectedData(file){
    let basename = path.basename(file.path)
    // console.log('path: ', file.path)
    // console.log('basename: ', basename)
    switch(basename){
        case 'file1.html':
            return {title: 'title 1'}
        case 'file3.html':
            return {title: 'title 3'}
        default: 
            return {}
    }
}

describe('Main', () => {

    describe('Integration', () => {
        
        it('should provide data to gulp-data', (done) => {
            gulp.src('test/files/**/*.html')
                // .pipe(debug())
                .pipe(data(json()))
                .pipe(assert.length(4))
                .pipe(assert.all((file) => {
                    let data = file.data
                    let expectedData = getExpectedData(file)
                    // console.log('data: ', JSON.stringify(data))
                    // console.log('expectedData: ', JSON.stringify(expectedData))
                    expect(data).to.deep.equals(expectedData)
                    
                }))
                .pipe(assert.end(done))
        })

    })

})
