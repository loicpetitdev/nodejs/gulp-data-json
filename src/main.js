let Context = require('./context')

module.exports = (options) => {
    return (file) => {
        return new Promise((resolve, reject) => {
            let ctx = new Context(options)
            ctx.log('options | ' + JSON.stringify(options))
            ctx.log('final options | ' + JSON.stringify(ctx.options))
            if(file){
                const jsonPath = ctx.getJsonPath(file.path)
                ctx.getFile(jsonPath).then((content) => {
                    let json = JSON.parse(content)
                    ctx.log(JSON.stringify(json))
                    resolve(json)
                }).catch((err) => {
                    ctx.log(err)
                    // file not mandatory, return empty data
                    resolve({})
                })
            }
            else {
                ctx.log('no file')
                // file not mandatory, return empty data
                resolve({})
            }
        })
    }
}