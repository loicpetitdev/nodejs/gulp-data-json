const expect = require('chai').expect
const sinon = require('sinon')
const sandbox = sinon.createSandbox()

describe('Main', () => {

    afterEach(() => {
        sandbox.restore()
    })

    describe('Module', () => {

        it('should get a function factory from module', () => {
            let module = require('./main')
            expect(module).to.be.a('function')
        })
    
        it('should get a function when executing module', () => {
            let module = require('./main')
            let options = {}
            let result = module(options)
            expect(result).to.be.a('function')
        })
    
        it('should return a promise when executing the generated function', () => {
            let module = require('./main')
            let options = {}
            let fct = module(options)
            let result = fct()
            expect(result).to.be.a('promise')
        })
        
    })

    describe('Function', () => {

        let Context = require('./context')
        let existingFile, notExistingFile, data

        beforeEach(() => {
            //  data
            existingFile = {
                path: './exists.json'
            }
            notExistingFile = {
                path: './not-exists.json'
            }
            data = {
                title: 'test title'
            }
            // mock
            let Context = require('./context')
            let getJsonPath = sandbox.stub(Context.prototype, 'getJsonPath').callsFake((filepath) => {
                return filepath
            })
            let getFile = sandbox.stub(Context.prototype, 'getFile').callsFake((jsonPath) => {
                switch(jsonPath){
                    case existingFile.path: return Promise.resolve(JSON.stringify(data))
                    default: return Promise.reject('not found')
                }
            })
        })

        it('should return the json data if found', () => {
            let fct = require('./main')()
            return fct(existingFile).then((result) => {
                expect(result).to.deep.equals(data)
            })
        })

        it('should return an empty object if no file', () => {
            let fct = require('./main')()
            return fct().then((result) => {
                expect(result).to.deep.equals({})
            })
        })

        it('should return an empty object if no json file found', () => {
            let fct = require('./main')()
            return fct(notExistingFile).then((result) => {
                expect(result).to.deep.equals({})
            })
        })

    })

})