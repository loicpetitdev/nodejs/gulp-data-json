const expect = require('chai').expect
const sinon = require('sinon')
const sandbox = sinon.createSandbox()
const path = require('path')

const Context = require('./context')

describe('Context', () => {

    afterEach(() => {
        sandbox.restore()
    })

    describe('Options', () => {

        it('should set default options', () => {
            let ctx = new Context()
            expect(ctx.options.debug).to.be.false
            expect(ctx.options.encoding).to.equals('utf-8')
            expect(ctx.options.src).to.be.null
            expect(ctx.options.relativeFrom).to.be.null
        })

        it('should override options', () => {
            let ctx = new Context({
                debug: true,
                encoding: 'ascii',
                src: '/temp',
                relativeFrom: 'project/json'
            })
            expect(ctx.options.debug).to.be.true
            expect(ctx.options.encoding).to.equals('ascii')
            expect(ctx.options.src).to.equals('/temp')
            expect(ctx.options.relativeFrom).to.equals('project/json')
        })

    })

    describe('Log', () => {

        let log

        beforeEach(() => {
            log = sandbox.stub(console, 'log')
        })

        it('should log if debug', () => {
            let ctx = new Context({
                debug: true
            })
            let msg = 'Debug mode !'
            ctx.log(msg)
            expect(log.calledOnce, 'called once').to.be.true
            let call = log.getCall(0)
            let callmsg = call.args[0]
            expect(callmsg, 'message').to.be.equals(`gulp-data-json | Debug mode !`)
        })

        it('should not log if no debug', () => {
            let ctx = new Context({
                debug: false
            })
            let msg = 'Debug mode !'
            ctx.log(msg)
            expect(log.calledOnce, 'called once').to.be.false
        })

    })

    describe('Get JSON path', () => {

        it('should convert next filepath by default', () => {
            let ctx = new Context()
            let jsonPath = ctx.getJsonPath('C:/folder/index.html')
            let expectedPath = path.normalize('C:/folder/index.json')
            expect(jsonPath).to.equals(expectedPath)
        })

        it('should convert in the src folder', () => {
            let ctx = new Context({src: 'C:/temp'})
            let jsonPath = ctx.getJsonPath('C:/folder/sub/index.html')
            let expectedPath = path.normalize('C:/temp/index.json')
            expect(jsonPath).to.equals(expectedPath)
        })

        it('should convert in the src folder relative from files folder', () => {
            let ctx = new Context({src: 'C:/temp', relativeFrom: 'C:/folder', debug: true})
            let jsonPath = ctx.getJsonPath('C:/folder/sub/index.html')
            let expectedPath = path.normalize('C:/temp/sub/index.json')
            expect(jsonPath).to.equals(expectedPath)
        })

        it('should throw error if no path', () => {
            let ctx = new Context()
            expect(() => ctx.getJsonPath()).to.throw('gulp-data-json | file path undefined')
        })

    })

    describe('Get file', () => {

        const existingPath = 'existingPath'
        const notExistingPath = 'notExistingPath'
        const errorPath = 'errorPath'
        const content = '{"title": "hello"}'
        const error = 'File error !'

        let fs, existsSync, readFile

        beforeEach(() => {
            // mock
            fs = require('fs')
            existsSync = sandbox.stub(fs, 'existsSync').callsFake((path) => {
                return path !== notExistingPath
            })
            readFile = sandbox.stub(fs, 'readFile').callsFake((path, encoding, callback) => {
                if(path !== errorPath){
                    callback(null, content)
                }
                else {
                    callback(error, null)
                }
            })
        })

        it('should get file', () => {
            let ctx = new Context()
            return ctx.getFile(existingPath).then((result) => {
                expect(result).to.equals(content)
            })
        })

        it('should reject if no path', () => {
            let ctx = new Context()
            return ctx.getFile().then(() => {
                throw 'Should not pass !'
            }).catch((err) => {
                expect(err).to.equals('Path is required')
            })
        })

        it('should reject if file not found', () => {
            let ctx = new Context()
            return ctx.getFile(notExistingPath).then(() => {
                throw 'Should not pass !'
            }).catch((err) => {
                expect(err).to.equals(`File ${notExistingPath} not found`)
            })
        })

        it('should reject if error during reading file', () => {
            let ctx = new Context()
            return ctx.getFile(errorPath).then(() => {
                throw 'Should not pass !'
            }).catch((err) => {
                expect(err).to.equals(error)
            })
        })

    })

})