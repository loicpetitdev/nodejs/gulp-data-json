const fs = require('fs')
const path = require('path')

/***
 * Context of the function created by the module, manage option dependent functionalities
 * Options :
 *  debug: true to display debug logs
 */
module.exports = class Context {
    
    constructor(options){
        this.options = Object.assign({
            debug: false,
            encoding: 'utf-8',
            src: null,
            relativeFrom: null
        }, options)
    }

    prefix(text) {
        if(text){
            return `gulp-data-json | ${text}`
        }
    }

    log(message) {
        if(this.options.debug){
            console.log(this.prefix(message))
        }
    }

    getJsonPath(filePath){
        //  filePath mandatory
        if(!filePath){
            let err = this.prefix('file path undefined')
            throw err
        }
        this.log(`filePath | ${filePath}`)
        // function to get the correct directory
        function getDir(filePath, src, relativeFrom){
            if(src && relativeFrom){
                let fileDir = path.dirname(filePath)
                let relativeDir = path.relative(relativeFrom, fileDir)
                return path.join(src, relativeDir)
            }
            else if(src){
                return src
            }
            else {
                return path.dirname(filePath)
            }
        }
        //  build the path
        const dir = getDir(filePath, this.options.src, this.options.relativeFrom)
        const ext = path.extname(filePath)
        const name = path.basename(filePath, ext)
        this.log(`dir | ${dir}`)
        this.log(`name | ${name}`)
        this.log(`ext | ${ext}`)
        const filename = `${name}.json`
        const jsonPath = path.join(dir, filename)
        this.log(`filename | ${filename}`)
        this.log(`jsonPath | ${jsonPath}`)
        return jsonPath
    }

    getFile(path){
        return new Promise((resolve, reject) => {
            if(!path){
                reject('Path is required')
            }
            if(fs.existsSync(path)){
                fs.readFile(path, this.encoding, (err, content) => {
                    if(!err){
                        resolve(content)
                    }
                    else {
                        reject(err)
                    }
                })
            }
            else {
                reject(`File ${path} not found`)
            }
        })
    }

}